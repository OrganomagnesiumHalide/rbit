use measurements::{Angle, Force, Length, Mass, Speed};
use serde::{Deserialize, Serialize};
use std::f64::consts::PI;
use std::iter;

use crate::{log, ImageData, Pos, Tracker, Velocity};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub(crate) enum CrashState {
    Landed(Tracker),
    Crashed(Tracker),
    Flying,
}
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub(crate) struct Object {
    name: String,
    object_type: ObjectType,
    pub(crate) position: Pos,
    mass: Mass,
    v: Velocity,
    force: Option<(f64, f64)>,
}

impl Object {
    pub(crate) fn new<T: Into<String>>(
        name: T,
        object_type: ObjectType,
        starting_position: Pos,
        mass: Mass,
        v: Velocity,
    ) -> Self {
        Self {
            name: name.into(),
            object_type,
            position: starting_position,
            mass,
            v,
            force: None,
        }
    }

    pub(crate) fn size_of_object(&self, scale: f64) -> u64 {
        match &self.object_type {
            ObjectType::Rocket(rocket) => (rocket.get_height().as_millimeters() / scale) as u64,
            ObjectType::Auto { radius, .. } => (radius.as_millimeters() / scale) as u64,
        }
    }

    pub(crate) fn get_object_position(
        &self,
        tracking_object: &Object,
        tracker: &Tracker,
        scale: f64,
        canvas_size: (i64, i64),
    ) -> Option<(i32, i32)> {
        let (x, y) = {
            let val_x: f64 = (self.position.x - tracking_object.position.x + tracker.offset.x)
                .as_millimeters()
                / scale;
            let val_y: f64 = (self.position.y - tracking_object.position.y + tracker.offset.y)
                .as_millimeters()
                / scale;
            (val_x, val_y)
        };
        let size = self.size_of_object(scale);
        if x as i64 - size as i64 > canvas_size.0
            || (y as i64 - size as i64) > canvas_size.1
            || (x as i64 + size as i64) < -(canvas_size.0)
            || (y as i64 + size as i64) < -(canvas_size.1)
        {
            None
        } else {
            Some((
                x as i32 + (canvas_size.0 / 2) as i32,
                y as i32 + (canvas_size.1 / 2) as i32,
            ))
        }
    }
    pub(crate) fn get_name(&self) -> String {
        return self.name.clone();
    }
    pub(crate) fn is_rocket(&self) -> bool {
        if let ObjectType::Rocket { .. } = self.object_type {
            true
        } else {
            false
        }
    }
    pub(crate) fn get_mut_rocket(&mut self) -> Option<&mut Rocket> {
        if let ObjectType::Rocket(rocket) = &mut self.object_type {
            Some(rocket)
        } else {
            None
        }
    }

    pub(crate) fn get_rocket(&self) -> Option<&Rocket> {
        if let ObjectType::Rocket(rocket) = &self.object_type {
            Some(rocket)
        } else {
            None
        }
    }

    pub(crate) fn get_speed(&self) -> (Speed, Speed) {
        (self.v.speed_x, self.v.speed_y)
    }

    pub(crate) fn set_speed(&mut self, dx: Speed, dy: Speed) {
        self.v.speed_x = dx;
        self.v.speed_y = dy;
    }

    pub(crate) fn get_mass(&self) -> Mass {
        if let ObjectType::Rocket(rocket) = &self.object_type {
            rocket.children.iter().map(|x| x.get_mass()).fold(
                self.mass + rocket.fuel_mass,
                |f, g| if g.as_kilograms() > 0. { f + g } else { f },
            )
        } else {
            self.mass
        }
    }

    pub(crate) fn get_image(&self, scale: f64) -> Vec<ImageData> {
        match &self.object_type {
            ObjectType::Auto { color, radius, .. } => {
                let radius = radius.as_millimeters() / scale;
                let radius = if radius > 2.0 { radius as u64 } else { 2 };
                vec![ImageData {
                    t: 0,
                    radius,
                    color: color.clone(),
                    size: 0,
                    ..Default::default()
                }]
            }
            ObjectType::Rocket(rocket) => {
                let height_of_rocket_pixels = rocket.get_height().as_millimeters() / scale;
                if height_of_rocket_pixels > 1. {
                    let mut offset_from_bottom = 0.;
                    let rocket_children = rocket.children.clone();

                    let mut children: Vec<Vec<(f64, f64)>> = rocket_children
                        .iter()
                        .rev()
                        .map(|x| x.shape.clone())
                        .collect();
                    children.push(rocket.shape.clone());
                    let direction = Angle::from_degrees(rocket.direction.as_degrees() + 180.);
                    children
                        .iter()
                        .enumerate()
                        .map(|(i, x)| {
                            let lowest = x
                                .iter()
                                .reduce(|accum, item| if accum.1 < item.1 { accum } else { item })
                                .unwrap()
                                .1;
                            let highest = x
                                .iter()
                                .reduce(|accum, item| if accum.1 > item.1 { accum } else { item })
                                .unwrap()
                                .1;

                            let old_offset_from_bottom = offset_from_bottom;

                            offset_from_bottom = old_offset_from_bottom + (highest - lowest);

                            let rocket_shape: Vec<(f64, f64)> = x
                                .into_iter()
                                .map(move |x| (x.0, x.1 + old_offset_from_bottom))
                                .collect();

                            let theta = rocket.direction.as_radians();
                            let rotation = vec![
                                ((theta + PI / 2.0).cos(), -((theta + PI / 2.0).sin())),
                                ((theta + PI / 2.0).sin(), (theta + PI / 2.0).cos()),
                            ];

                            let mut points = Vec::with_capacity(rocket_shape.len());
                            let mut direction_dist = 0.;
                            for mut row in rocket_shape {
                                row = matrix_dot_product(row, &rotation);
                                row.0 *= 1. / (scale / 1000.);
                                row.1 *= 1. / (scale / 1000.);
                                let local_direction_dist = (row.0 * row.0 + row.1 * row.1).sqrt();
                                if direction_dist < local_direction_dist {
                                    direction_dist = local_direction_dist;
                                }
                                points.push(row.0 as i32);
                                points.push(row.1 as i32);
                            }
                            direction_dist += 10.0;
                            let direction_x = -direction.as_radians().cos() * direction_dist;
                            let direction_y = direction.as_radians().sin() * direction_dist;

                            let show_guide = i == 0;
                            ImageData {
                                t: 1,
                                points,
                                direction_dist,
                                direction_x,
                                direction_y,
                                show_guide,
                                ..Default::default()
                            }
                        })
                        .collect()
                } else {
                    vec![ImageData {
                        t: 1,
                        points: vec![],
                        direction_dist: 10.,
                        direction_x: -10. * (rocket.direction.as_radians() + PI).cos(),
                        direction_y: 10. * (rocket.direction.as_radians() + PI).sin(),
                        show_guide: true,
                        ..Default::default()
                    }]
                }
            }
        }
    }

    pub(crate) fn get_new_velocity(&self, force: (f64, f64), tick_time: f64) -> (Speed, Speed) {
        let f_x = force.0;
        let f_y = force.1;
        let new_speed_x = self.v.speed_x
            + Speed::from_meters_per_second(f_x / self.get_mass().as_kilograms() * tick_time);
        let new_speed_y = self.v.speed_y
            + Speed::from_meters_per_second(f_y / self.get_mass().as_kilograms() * tick_time);
        (new_speed_x, new_speed_y)
    }
    pub(crate) fn update_pos(&mut self, tick_time: f64) {
        if self.is_crashed().is_none() && self.is_landed().is_none() {
            self.position.x = self.position.x
                + Length::from_meters(self.v.speed_x.as_meters_per_second() * tick_time);
            self.position.y = self.position.y
                + Length::from_meters(self.v.speed_y.as_meters_per_second() * tick_time);
        }
    }
    pub(crate) fn set_pos_tracker(&mut self, tracker: &Object, offset: (Length, Length)) {
        self.position.x = tracker.position.x + offset.0;
        self.position.y = tracker.position.y + offset.1;
    }
    pub(crate) fn is_crashed(&self) -> Option<Tracker> {
        if let ObjectType::Rocket(rocket) = &self.object_type {
            rocket.is_crashed()
        } else {
            None
        }
    }

    pub(crate) fn is_flying(&self) -> bool {
        if let ObjectType::Rocket(rocket) = &self.object_type {
            rocket.is_flying()
        } else {
            false
        }
    }
    pub(crate) fn is_landed(&self) -> Option<Tracker> {
        if let ObjectType::Rocket(rocket) = &self.object_type {
            rocket.is_landed()
        } else {
            None
        }
    }
    pub(crate) fn set_if_crashed(objects: &mut [Object], index_to_set: usize) {
        // Unfortunately, I need to do this to avoid allocating and deallocating in a hot loop
        let rocket = if let ObjectType::Rocket(rocket) = &objects[index_to_set].object_type {
            rocket
        } else {
            return;
        };
        let absolute_coordinates_of_vertices = rocket.get_absolute_coordinates_of_vertices(
            objects[index_to_set].position.x,
            objects[index_to_set].position.y,
        );
        let did_touch = objects
            .iter()
            .enumerate()
            .filter_map(|(i, object)| {
                if let ObjectType::Auto {
                    radius, landable, ..
                } = object.object_type
                {
                    let x = objects[i].position.x;
                    let y = objects[i].position.y;
                    for (vertex_x, vertex_y) in &absolute_coordinates_of_vertices {
                        let dx = (*vertex_x - x).as_meters();
                        let dy = (*vertex_y - y).as_meters();
                        let distance = (dx * dx + dy * dy).sqrt();
                        if distance <= radius.as_meters().abs() {
                            let radians_from_center = (dy.atan2(dx) + 2. * PI) % (2. * PI);
                            let rocket_diff =
                                (-rocket.direction.as_radians() + 2. * PI) % (2. * PI);
                            let deg_diff = rocket_diff - radians_from_center;
                            let old_vec_x = (objects[index_to_set].v.speed_x
                                - objects[i].v.speed_x)
                                .as_meters_per_second();
                            let old_vec_y = (objects[index_to_set].v.speed_y
                                - objects[i].v.speed_y)
                                .as_meters_per_second();
                            let v = (old_vec_x * old_vec_x + old_vec_y * old_vec_y).sqrt();
                            let theta =
                                ((old_vec_y.atan2(old_vec_x) + 2. * PI) % (2. * PI)) - (PI / 2.);
                            let new_vec_x = v * theta.cos();
                            let new_vec_y = v * theta.sin();
                            let did_land = if landable
                                && PI - (deg_diff.abs() - PI).abs() < crate::RADIANS_TO_CRASH
                                && new_vec_x < crate::CRASH_VELOCITY_X
                                && new_vec_y < crate::CRASH_VELOCITY_Y
                            {
                                true
                            } else {
                                false
                            };

                            return Some((dx, dy, i, did_land, radius));
                        }
                    }
                    return None;
                } else {
                    None
                }
            })
            .next();
        if let Some((dx, dy, i, did_land, radius)) = did_touch {
            let rocket = if let ObjectType::Rocket(rocket) = &mut objects[index_to_set].object_type
            {
                rocket
            } else {
                return;
            };
            let radians_from_center = (dy.atan2(dx) + 2. * PI) % (2. * PI);
            let dx = radius * radians_from_center.cos();
            let dy = radius * radians_from_center.sin();
            if did_land {
                rocket.crash_state = CrashState::Landed(Tracker {
                    object: i,
                    offset: Pos { x: dx, y: dy },
                });
            } else {
                rocket.crash_state = CrashState::Crashed(Tracker {
                    object: i,
                    offset: Pos { x: dx, y: dy },
                });
            }
        } else {
            let rocket = if let ObjectType::Rocket(rocket) = &mut objects[index_to_set].object_type
            {
                rocket
            } else {
                return;
            };
            rocket.crash_state = CrashState::Flying;
        }
    }
    pub(crate) fn get_position(&self) -> Pos {
        self.position
    }
    pub(crate) fn get_velocity(&self) -> Velocity {
        self.v
    }
    pub(crate) fn set_force(&mut self, force: (f64, f64)) {
        self.force = Some(force);
    }
    pub(crate) fn get_force(&self) -> Option<(f64, f64)> {
        self.force
    }
    pub(crate) fn stage(&mut self) -> Option<Object> {
        if let ObjectType::Rocket(rocket) = &mut self.object_type {
            rocket.stage(&mut self.position, self.mass, self.v.clone())
        } else {
            unreachable!();
        }
    }
    pub(crate) fn get_radius(&self) -> Option<Length> {
        if let ObjectType::Auto { radius, .. } = self.object_type {
            Some(radius)
        } else {
            None
        }
    }
}
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]

pub(crate) enum ObjectType {
    Rocket(Rocket),
    Auto {
        controller: usize,
        radius: Length,
        color: String,
        landable: bool,
    },
}
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]

pub(crate) struct Rocket {
    direction: measurements::Angle,
    height: measurements::Length,
    main_engine_thrust: measurements::Force,
    side_engine_thrust: measurements::Force,
    fuel_mass: Mass,
    fuel_flow_in_kg_per_second: u64,
    crash_state: CrashState,
    shape: Vec<(f64, f64)>,
    children: Vec<RocketComponent>,
}
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]

pub(crate) struct RocketComponent {
    pub(crate) name: String,
    pub(crate) mass: Mass,
    pub(crate) main_engine_thrust: measurements::Force,
    pub(crate) side_engine_thrust: measurements::Force,
    pub(crate) fuel_flow_in_kg_per_second: u64,
    pub(crate) fuel_mass: Mass,
    pub(crate) shape: Vec<(f64, f64)>,
}

impl RocketComponent {
    pub(crate) fn get_mass(&self) -> Mass {
        self.mass + self.fuel_mass
    }
    pub(crate) fn get_height(&self) -> Length {
        let largest = self
            .shape
            .iter()
            .fold(f64::MIN, |a, (_, y)| if a > *y { a } else { *y });
        let smallest = self
            .shape
            .iter()
            .fold(f64::MAX, |a, (_, y)| if a < *y { a } else { *y });
        Length::from_meters(largest - smallest)
    }
}

impl Rocket {
    pub(crate) fn new(
        direction: Angle,
        main_engine_thrust: Force,
        side_engine_thrust: Force,
        fuel_mass: Mass,
        crash_state: CrashState,
        fuel_flow_in_kg_per_second: u64,
        shape: Vec<(f64, f64)>,
        children: Vec<RocketComponent>,
    ) -> Self {
        let largest = shape
            .iter()
            .fold(f64::MIN, |a, (_, y)| if a > *y { a } else { *y });
        let smallest = shape
            .iter()
            .fold(f64::MAX, |a, (_, y)| if a < *y { a } else { *y });
        let height = Length::from_meters(largest - smallest);
        Self {
            direction,
            height,
            main_engine_thrust,
            side_engine_thrust,
            fuel_mass,
            children,
            shape,
            fuel_flow_in_kg_per_second,
            crash_state,
        }
    }
    pub(crate) fn get_direction(&self) -> Angle {
        self.direction
    }

    pub(crate) fn set_direction(&mut self, angle: Angle) {
        self.direction = angle
    }
    pub(crate) fn get_main_engine_thrust(&self) -> Force {
        if self.children.len() == 0 {
            self.main_engine_thrust
        } else {
            self.children[self.children.len() - 1].main_engine_thrust
        }
    }
    pub(crate) fn get_fuel_mass(&self) -> Mass {
        if self.children.len() == 0 {
            self.fuel_mass
        } else {
            self.children[self.children.len() - 1].fuel_mass
        }
    }
    pub(crate) fn burn(&mut self, time_in_seconds: f64) {
        let burned_kg = self.get_fuel_flow_in_kg_per_second() * time_in_seconds;

        let len_of_children = self.children.len();
        if len_of_children == 0 {
            self.fuel_mass = self.fuel_mass - Mass::from_kilograms(burned_kg);
        } else {
            self.children[len_of_children - 1].fuel_mass =
            self.children[len_of_children - 1].fuel_mass - Mass::from_kilograms(burned_kg);
        }
    }

    pub(crate) fn get_fuel_flow_in_kg_per_second(&self) -> f64 {
        if self.children.len() == 0 {
            self.fuel_flow_in_kg_per_second as f64
        } else {
            self.children[self.children.len() - 1].fuel_flow_in_kg_per_second as f64
        }
    }

    pub(crate) fn get_side_engine_thrust(&self) -> Force {
        if self.children.len() == 0 {
            self.side_engine_thrust
        } else {
            self.children[self.children.len() - 1].side_engine_thrust
        }
    }

    pub(crate) fn get_height(&self) -> Length {
        self.children
            .iter()
            .fold(self.height, |a, b| a + b.get_height())
    }
    pub(crate) fn stage(&mut self, pos: &mut Pos, mass: Mass, v: Velocity) -> Option<Object> {
        let last_child = self.children.pop();
        if let Some(last_child) = last_child {
            let new_pos = pos.clone();
            pos.x = new_pos.x
                + Length::from_meters(
                    last_child.get_height().as_meters() * self.direction.as_radians().cos(),
                );
            pos.y = new_pos.y
                - Length::from_meters(
                    last_child.get_height().as_meters() * self.direction.as_radians().sin(),
                );
            Some(Object::new(
                last_child.name.clone(),
                ObjectType::Rocket(Rocket {
                    direction: self.direction,
                    height: last_child.get_height(),
                    main_engine_thrust: last_child.main_engine_thrust,
                    side_engine_thrust: last_child.side_engine_thrust,
                    fuel_flow_in_kg_per_second: last_child.fuel_flow_in_kg_per_second,
                    fuel_mass: last_child.fuel_mass,
                    crash_state: CrashState::Flying,
                    shape: last_child.shape,
                    children: vec![],
                }),
                new_pos,
                mass,
                v,
            ))
        } else {
            None
        }
    }
    pub(crate) fn get_absolute_coordinates_of_vertices(
        &self,
        x: Length,
        y: Length,
    ) -> Vec<(Length, Length)> {
        self.children
            .iter()
            .map(|rocket_component| &rocket_component.shape)
            .chain(iter::once(&self.shape))
            .map(|rocket_component_shape| {
                let theta = self.direction.as_radians();
                let rotation = vec![
                    ((theta + PI / 2.0).cos(), -((theta + PI / 2.0).sin())),
                    ((theta + PI / 2.0).sin(), (theta + PI / 2.0).cos()),
                ];

                let mut points = Vec::with_capacity(rocket_component_shape.len());
                for row in rocket_component_shape {
                    let row = matrix_dot_product(*row, &rotation);
                    points.push((
                        x + Length::from_meters(row.0),
                        y + Length::from_meters(row.1),
                    ));
                }
                points
            })
            .flatten()
            .collect()
    }
    pub(crate) fn set_flying(&mut self) {
        self.crash_state = CrashState::Flying;
    }

    pub(crate) fn is_crashed(&self) -> Option<Tracker> {
        if let CrashState::Crashed(tracker) = &self.crash_state {
            Some(tracker.clone())
        } else {
            None
        }
    }

    pub(crate) fn is_flying(&self) -> bool {
        if self.crash_state == CrashState::Flying {
            true
        } else {
            false
        }
    }
    pub(crate) fn is_landed(&self) -> Option<Tracker> {
        if let CrashState::Landed(tracker) = &self.crash_state {
            Some(tracker.clone())
        } else {
            None
        }
    }
}

fn matrix_dot_product<T>(matrix_1: (T, T), matrix2: &Vec<(T, T)>) -> (T, T)
where
    T: Copy + std::ops::Mul<Output = T> + std::default::Default + std::ops::Add<Output = T>,
{
    (
        matrix_1.0 * matrix2[0].0 + matrix_1.1 * matrix2[1].0,
        matrix_1.0 * matrix2[0].1 + matrix_1.1 * matrix2[1].1,
    )
}
mod test {

    use measurements::{Length, Mass, Speed};

    use crate::{Pos, Universe, Velocity, AU};

    use super::{Object, ObjectType};
    macro_rules! assert_delta {
        ($x:expr, $y:expr, $d:expr) => {
            if !($x - $y < $d || $y - $x < $d) {
                panic!("{}", $x - $y);
            }
        };
    }

    fn get_universe() -> Universe {
        let mut u = Universe::init(10, 1000000000000, 100, 100, 1000 * 60 * 60);
        u.objects = //u.objects.split_at(4).0.to_vec();
         vec![
            Object {
                name: "Sun".to_string(),
                force: None,
                object_type: ObjectType::Auto {
                    controller: 0,
                    radius: Length::from_kilometers(100.),
                    color: "Yellow".to_string(),
                    landable: false
                },
                position: Pos {
                    x: Length::from_meters(0.),
                    y: Length::from_meters(0.),
                },
                mass: Mass::from_kilograms(1.98892e30),
                v: Velocity {
                    speed_x: Speed::from_kilometers_per_hour(0.),
                    speed_y: Speed::from_kilometers_per_hour(0.),
                },
            },
            Object {
                name: "Mercury".to_string(),
                force: None,
                object_type: ObjectType::Auto {
                    controller: 0,
                    radius: Length::from_kilometers(100.),
                    color: "Yellow".to_string(),
                    landable: true
                },
                position: Pos {
                    x: Length::from_meters(0.723 * AU),
                    y: Length::from_meters(0.),
                },
                mass: Mass::from_kilograms(0.1),
                v: Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(-35.02 * 1000.*1000.),
                },
            },
            Object {
                name: "Venus".to_string(),
                force: None,
                object_type: ObjectType::Auto {
                    controller: 0,
                    radius: Length::from_kilometers(100.),
                    color: "Yellow".to_string(),
                    landable: true
                },
                position: Pos {
                    x: Length::from_meters(0.723 * AU),
                    y: Length::from_meters(0.),
                },
                mass: Mass::from_kilograms(4.8685e24),
                v: Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(-35.02 * 1000.),
                },
            },
            Object {
                name: "Earth".to_string(),
                force: None,
                object_type: ObjectType::Auto {
                    controller: 0,
                    radius: Length::from_kilometers(100.),
                    color: "Yellow".to_string(),
                    landable: false,
                },
                position: Pos {
                    x: Length::from_meters(-1.0 * AU),
                    y: Length::from_meters(0.),
                },
                mass: Mass::from_kilograms(5.9742e24),
                v: Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(29.783 * 1000.),
                },
            },
        ];
        u
    }

    #[test]
    fn check_calculate_gravitational_force_in_newtons() {
        use crate::get_gravitational_force_in_newtons;
        let u = get_universe();
        let (fx, fy) = get_gravitational_force_in_newtons(
            u.objects[3].mass,
            u.objects[0].mass,
            u.objects[3].position,
            u.objects[0].position,
        );

        assert_eq!(fx, 35435478992991075000000.0);
        assert_eq!(fy, 0.);
    }
    #[test]
    fn check_get_net_force_earth() {
        use crate::get_net_force;

        let u = get_universe();
        let (fx, fy) = get_net_force(&u.objects, 3);

        assert_eq!(fx, 35435508210663265000000.0);
        assert_eq!(fy, 0.);
    }
    #[test]
    fn check_get_net_force_venus() {
        use crate::get_net_force;
        let u = get_universe();
        let (fx, fy) = get_net_force(&u.objects, 2);

        assert_eq!(fx, -55243013284844340000000.0);
        assert_eq!(fy, 6765317.939453933);
    }

    #[test]
    fn check_get_change_velocity_earth() {
        use crate::get_net_force;
        let u = get_universe();

        let (fx, fy) = get_net_force(&u.objects, 3);
        let (dx, dy) = u.objects[3].get_new_velocity((fx, fy), 24. * 3600.);
        assert_eq!(dx.as_meters_per_second(), 512.4749605639761);
        assert_eq!(dy.as_meters_per_second(), 29783.0);
    }

    #[test]
    fn check_tick() {
        let mut u = get_universe(); //Universe::init(10, 1000000000000, 100, 100, 1000 * 60 * 60);
        for _ in 0..10_000 {
            u.should_move = true;
            if !u.should_move {
                panic!("isn't moving");
            }
            u.tick();

            assert!(u.objects[3].position.x.as_meters() < 2.0 * AU);
            assert!(u.objects[3].position.y.as_meters() < 2.0 * AU);
        }
    }
    #[test]
    fn check_update_pos_range_check() {
        use crate::get_net_force;
        let mut u = get_universe();
        for i in 0..1_000_000 {
            for p in 0..4 {
                let (fx, fy) = get_net_force(&u.objects, p);
                let (dx, dy) = u.objects[p].get_new_velocity((fx, fy), 24. * 3600.);
                u.objects[p].v.speed_x = dx;
                u.objects[p].v.speed_y = dy;
                u.objects[p].update_pos(24. * 3600.);
                eprintln!("{} {} {}", i, p, u.objects[p].position.y.as_meters() / AU);
                assert!(u.objects[p].position.x.as_meters() < 2.0 * AU);
                assert!(u.objects[p].position.y.as_meters() < 2.0 * AU);
            }
        }
    }
    #[test]
    fn check_update_pos() {
        use crate::get_net_force;

        let mut u = get_universe();
        for i in 0..1_000_000 {
            for p in 0..4 {
                let (fx, fy) = get_net_force(&u.objects, p);
                let (dx, dy) = u.objects[p].get_new_velocity((fx, fy), 24. * 3600.);
                u.objects[p].v.speed_x = dx;
                u.objects[p].v.speed_y = dy;
                u.objects[p].update_pos(24. * 3600.);
                if i == 0 {
                    assert_delta!(u.objects[3].position.x.as_meters(), -1.00 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.x.as_meters(), 0.72 * AU, 0.01 * AU);

                    assert_delta!(u.objects[3].position.y.as_meters(), 0. * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.y.as_meters(), 0. * AU, 0.01 * AU);
                }
                if i == 10 {
                    assert_delta!(u.objects[3].position.x.as_meters(), -0.99 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.x.as_meters(), 0.70 * AU, 0.01 * AU);

                    assert_delta!(u.objects[3].position.y.as_meters(), 0.15 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.y.as_meters(), -0.18 * AU, 0.01 * AU);
                }
                if i == 50 {
                    assert_delta!(u.objects[3].position.x.as_meters(), -0.66 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.x.as_meters(), 0.13 * AU, 0.01 * AU);

                    assert_delta!(u.objects[3].position.y.as_meters(), 0.75 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.y.as_meters(), -0.70 * AU, 0.01 * AU);
                }
                if i == 100 {
                    assert_delta!(u.objects[3].position.x.as_meters(), 0.15 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.x.as_meters(), -0.68 * AU, 0.01 * AU);

                    assert_delta!(u.objects[3].position.y.as_meters(), 0.98 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.y.as_meters(), -0.22 * AU, 0.01 * AU);
                }
                if i == 500 {
                    assert_delta!(u.objects[3].position.x.as_meters(), 0.15 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.x.as_meters(), -0.68 * AU, 0.01 * AU);

                    assert_delta!(u.objects[3].position.y.as_meters(), 0.98 * AU, 0.01 * AU);
                    assert_delta!(u.objects[2].position.y.as_meters(), -0.22 * AU, 0.01 * AU);
                }
            }
        }
    }
}
