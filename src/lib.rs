mod objects;
mod utils;
use measurements::{Angle, Force, Length, Mass, Speed};

use objects::{CrashState, Object, ObjectType, Rocket, RocketComponent};
use serde::{Deserialize, Serialize};
use serde_json::json;

use std::ops::Sub;
use wasm_bindgen::prelude::*;
extern crate web_sys;

// A macro to provide `println!(..)`-style syntax for `console.log` logging.
#[macro_export]
macro_rules! log {
    ( $( $t:tt )* ) => {
        web_sys::console::log_1(&format!( $( $t )* ).into());
    }
}
const G: f64 = 6.67428e-11;
const AU: f64 = 149.6e6 * 1000.;
pub(crate) const RADIANS_TO_CRASH: f64 = std::f64::consts::PI / 12.0;
pub(crate) const CRASH_VELOCITY_X: f64 = 10.; //10 mps
pub(crate) const CRASH_VELOCITY_Y: f64 = 30.; //30 mps

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
// #[cfg(feature = "wee_alloc")]
// #[global_allocator]
// static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Universe {
    track: Tracker,
    objects: Vec<Object>,
    should_move: bool,
    time_ms: u64,
    scale: u64,
    canvas_size: (u32, u32),
    tick_time: u64,
    where_mouse_down: Option<Pos>,
    focus: Option<usize>,
    view: ViewState,
    buttons_pressed: Vec<Button>,
    velocity_to: Option<usize>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
enum Button {
    ArrowUp,
    ArrowDown,
    ArrowRight,
    ArrowLeft,
    PanRight,
    PanLeft,
    Stage,
    Space,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct ScreenPos {
    x: u32,
    y: u32,
}
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub(crate) struct Tracker {
    object: usize,
    offset: Pos,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[wasm_bindgen]
pub struct ReturnPos {
    pub x: i32,
    pub y: i32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]

struct Velocity {
    speed_x: measurements::Speed,
    speed_y: measurements::Speed,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
enum ViewState {
    Help,
    Load { date_time: u64 },
    Main,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[wasm_bindgen]
pub struct Pos {
    x: Length,
    y: Length,
}

impl Pos {
    fn xy(x: f64, y: f64) -> Self {
        Pos {
            x: Length::from_meters(x),
            y: Length::from_meters(y),
        }
    }
}
impl Sub for Pos {
    type Output = Pos;

    fn sub(self, rhs: Self) -> Self::Output {
        Pos {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

#[wasm_bindgen]
impl Universe {
    pub fn init(
        time_seconds: u64,
        scale: u64,
        canvas_length: u32,
        canvas_width: u32,
        tick_time: u64,
    ) -> Universe {
        utils::set_panic_hook();

        // Not so accurate since orbits are elliptic and not always is the major axis on the same point in Y.
        // Source is https://science.nasa.gov/science-news/science-at-nasa/2001/ast04jan_1
        // https://nssdc.gsfc.nasa.gov/planetary/factsheet/
        // colors are from www.schemecolor.com
        let objects = vec![
            Object::new(
                "Sun",
                ObjectType::Auto {
                    controller: 0,
                    color: String::from("#FFCC33"),
                    radius: Length::from_kilometers(695_660.),
                    landable: false,
                },
                Pos::xy(0., 0.),
                Mass::from_kilograms(1.98892e30),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.0),
                    speed_y: Speed::from_meters_per_second(0.0),
                },
            ),
            Object::new(
                "Mercury",
                ObjectType::Auto {
                    controller: 0,
                    color: String::from("#97979F"),
                    radius: Length::from_kilometers(2_439.7),
                    landable: true,
                },
                Pos::xy(0.313 * AU, 0.),
                Mass::from_kilograms(0.33010e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(59.98 * 1000.),
                },
            ),
            Object::new(
                "Venus",
                ObjectType::Auto {
                    controller: 0,
                    color: String::from("#DDD8D4"),
                    radius: Length::from_kilometers(6051.8),
                    landable: true,
                },
                Pos::xy(0.731 * AU, 0.),
                Mass::from_kilograms(4.8674e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(35.26 * 1000.),
                },
            ),
            Object::new(
                "Earth",
                ObjectType::Auto {
                    controller: 0,
                    color: String::from("#3C4258"),
                    radius: Length::from_kilometers(6_356.8),
                    landable: true,
                },
                Pos::xy(0.98 * AU, 0.),
                Mass::from_kilograms(5.9722e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(30.29 * 1000.),
                },
            ),
            Object::new(
                "Mars",
                ObjectType::Auto {
                    controller: 0,
                    color: String::from("#C67B5C"),
                    radius: Length::from_kilometers(3376.2),
                    landable: true,
                },
                Pos::xy(1.38 * AU, 0.),
                Mass::from_kilograms(0.64169e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(26.50 * 1000.),
                },
            ),
            Object::new(
                "Jupiter",
                ObjectType::Auto {
                    controller: 0,

                    color: String::from("#90614D"),
                    radius: Length::from_kilometers(66_854.),
                    landable: false,
                },
                Pos::xy(4.95 * AU, 0.),
                Mass::from_kilograms(1898e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(13.72 * 1000.),
                },
            ),
            Object::new(
                "Saturn",
                ObjectType::Auto {
                    controller: 0,
                    color: String::from("#C5AB6E"),
                    radius: Length::from_kilometers(58_232.),
                    landable: false,
                },
                Pos::xy(9.02 * AU, 0.),
                Mass::from_kilograms(568.32e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(10.18 * 1000.),
                },
            ),
            Object::new(
                "Uranus",
                ObjectType::Auto {
                    controller: 0,
                    color: String::from("#93B8BE"),
                    radius: Length::from_kilometers(24_973.),
                    landable: false,
                },
                Pos::xy(18.3 * AU, 0.),
                Mass::from_kilograms(86.811e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(7.11 * 1000.),
                },
            ),
            Object::new(
                "Neptune",
                ObjectType::Auto {
                    controller: 0,
                    color: String::from("#3E66F9"),
                    radius: Length::from_kilometers(24_764.),
                    landable: false,
                },
                Pos::xy(30.0 * AU, 0.),
                Mass::from_kilograms(102.409e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(5.5 * 1000.),
                },
            ),
            Object::new(
                "Moon",
                ObjectType::Auto {
                    controller: 3,
                    color: String::from("#A1A19C"),
                    radius: Length::from_kilometers(1_737.),
                    landable: true,
                },
                Pos::xy(0.98 * AU + 0.002569 * AU + 6_371_071.03, 0.),
                Mass::from_kilograms(0.07346e24),
                Velocity {
                    speed_x: Speed::from_meters_per_second(0.),
                    speed_y: Speed::from_meters_per_second(30.29 * 1000. + 1022.0),
                },
            ),
            Object::new(
                "Dragon 2",
                ObjectType::Rocket(Rocket::new(
                    Angle::from_degrees(0.),
                    Force::from_newtons(73_000.), // Thrust of SuperDracos
                    Force::from_newtons(400.),    // Thrust of Dracos
                    Mass::from_kilograms(0.),     // Don't know fuel mass
                    CrashState::Flying,
                    0, // Don't know flow rate
                    vec![(-2., 0.), (0., 5.), (2., 0.)],
                    vec![
                        RocketComponent {
                            name: "Dragon trunk".into(),
                            //https://spaceflightnow.com/falcon9/009/dragon.html#:~:text=The%20trunk%20is%202.8%20meters,heat%20shield%20in%20the%20world.
                            main_engine_thrust: Force::from_newtons(73_000.), // We'll just consider the thrusters to be attached here (even through they are not).
                            side_engine_thrust: Force::from_newtons(400.), // We'll just consider the thrusters to be attached here (even through they are not).
                            fuel_flow_in_kg_per_second: 0,                 // Don't know flow rate
                            fuel_mass: Mass::from_kilograms(0.),           // Don't know fuel mass
                            shape: vec![(-2., 0.), (2.0, 0.), (2., 4.), (-2., 4.)],
                            mass: Mass::from_kilograms(3_900.),
                        },
                        //http://www.spacelaunchreport.com/falcon9ft.html
                        RocketComponent {
                            // Stage 2 Falcon FT
                            name: "Falcon Second Stage".into(),
                            main_engine_thrust: Force::from_newtons(934_000.),
                            side_engine_thrust: Force::from_newtons(400.), // Assuming its the same Dracos as on the Dragon
                            fuel_mass: Mass::from_kilograms(92_670.),
                            fuel_flow_in_kg_per_second: (934_000. / (340. * 9.81)) as u64, //based on https://space.stackexchange.com/questions/30497/does-anyone-know-how-to-calculate-the-kg-per-second-of-rp-1-and-lox-from-the-9-m/30498
                            shape: vec![(-2., 0.), (-2., 16.), (2., 16.), (2., 0.)],
                            mass: Mass::from_kilograms(3_900.),
                        },
                        RocketComponent {
                            // Stage 1 Falcon FT
                            name: "Falcon First Stage".into(),
                            main_engine_thrust: Force::from_newtons(8_227_000.),
                            side_engine_thrust: Force::from_newtons(400.), // Assuming its the same Dracos as on the Dragon
                            fuel_mass: Mass::from_kilograms(395_700.),
                            fuel_flow_in_kg_per_second: (8_227_000. / (311. * 9.81)) as u64, //based on https://space.stackexchange.com/questions/30497/does-anyone-know-how-to-calculate-the-kg-per-second-of-rp-1-and-lox-from-the-9-m/30498
                            shape: vec![(-2., 0.), (-2., 40.), (2., 40.), (2., 0.)],
                            mass: Mass::from_kilograms(25_600.),
                        },
                    ],
                )), //Parking orbit is the ISS orbit
                Pos::xy(0.98 * AU + 6356.85 * 1000. - 50., 0.),
                Mass::from_kilograms(12_055.0), // https://twitter.com/StephenClark1/status/1101504286192742400
                Velocity {
                    speed_y: Speed::from_meters_per_second(30.29 * 1000.),
                    speed_x: Speed::from_meters_per_second(0.),
                },
            ),
        ];
        let focus = objects
            .iter()
            .enumerate()
            .find(|(_, x)| x.is_rocket())
            .map(|x| x.0);
        Universe {
            track: Tracker {
                object: 10,
                offset: Pos::xy(0., 0.),
            }, //SUN
            objects,
            should_move: false,
            time_ms: time_seconds,
            scale,
            canvas_size: (canvas_length, canvas_width),
            tick_time,
            where_mouse_down: None,
            focus: focus,
            view: ViewState::Main,
            buttons_pressed: vec![],
            velocity_to: None,
        }
    }

    fn size_of_object(&self, object: usize) -> u64 {
        self.objects[object].size_of_object(self.scale as f64)
    }
    pub fn get_object_position(&self, object: usize) -> Option<ReturnPos> {
        let p = &self.objects[object];
        p.get_object_position(
            &self.objects[self.track.object],
            &self.track,
            self.scale as f64,
            (self.canvas_size.0.into(), self.canvas_size.1.into()),
        )
        .map(|x| ReturnPos { x: x.0, y: x.1 })
    }
    pub fn find_object(&self, x: i32, y: i32) -> Vec<usize> {
        let mut return_planets: Vec<(usize, f64)> = vec![];
        for i in 0..self.objects.len() {
            if let Some(v) = self.get_object_position(i) {
                if (v.x + 10).checked_sub(x).is_some() && (v.y + 10).checked_sub(y).is_some() {
                    if (v.x + 10 - x).abs() <= (20 + self.size_of_object(i)).try_into().unwrap()
                        && (v.y + 10 - y).abs() <= (20 + self.size_of_object(i)).try_into().unwrap()
                    {
                        let d = distance((v.x - x).abs() as f64, (v.y - y).abs() as f64);
                        return_planets.push((i, d));
                    }
                }
            }
        }
        return_planets.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        return_planets
            .iter()
            .map(|(planets, _)| planets.clone())
            .collect()
    }
    pub fn context_menu_to_show(&mut self, x: i32, y: i32) -> Option<String> {
        if self.view == ViewState::Main {
            let mut return_val = "".to_string();
            self.should_move = false;
            for o in self.find_object(x, y) {
                return_val +=
                    &format!("Center at {}|center_at({});", self.objects[o].get_name(), o);
            }
            for o in self.find_object(x, y) {
                if let Some(focus) = self.focus {
                    if focus != o {
                        return_val += &format!(
                            "Velocity relative to {}|v_to({});",
                            self.objects[o].get_name(),
                            o
                        );
                    }
                }
            }
            if return_val == "" {
                None
            } else {
                Some(return_val)
            }
        } else {
            None
        }
    }
    pub fn center_at(&mut self, o: usize) {
        if self.view == ViewState::Main {
            self.track = Tracker {
                object: o,
                offset: Pos::xy(0., 0.),
            };
        }
    }
    pub fn v_to(&mut self, o: usize) {
        if self.view == ViewState::Main {
            self.velocity_to = Some(o);
        }
    }
    pub fn set_canvas_size(&mut self, l: u32, w: u32) {
        self.canvas_size = (l, w);
    }
    pub fn tick(&mut self) {
        let multiplyer = 10;
        for _ in 0..multiplyer {
            for buttons_pressed in self.buttons_pressed.iter() {
                if let Some(focus) = self.focus {
                    let mass = self.objects[focus].get_mass().as_kilograms() as f64;

                    if let Some(rocket) = self.objects[focus].get_mut_rocket() {
                        match buttons_pressed {
                            Button::ArrowUp => {
                                let direction = rocket.get_direction();
                                let a = rocket.get_side_engine_thrust().as_newtons() / mass;
                                let a_x = a * direction.as_radians().cos();
                                let a_y = a * direction.as_radians().sin();
                                let (speed_x, speed_y) = self.objects[focus].get_speed();
                                self.objects[focus].set_speed(
                                    speed_x
                                        + Speed::from_meters_per_second(
                                            a_x * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                    speed_y
                                        - Speed::from_meters_per_second(
                                            a_y * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                );
                            }
                            Button::ArrowDown => {
                                let direction =
                                    Angle::from_degrees(rocket.get_direction().as_degrees() + 180.);
                                let a = rocket.get_side_engine_thrust().as_newtons() / mass;
                                let a_x = a * direction.as_radians().cos();
                                let a_y = a * direction.as_radians().sin();
                                let (speed_x, speed_y) = self.objects[focus].get_speed();
                                self.objects[focus].set_speed(
                                    speed_x
                                        + Speed::from_meters_per_second(
                                            a_x * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                    speed_y
                                        - Speed::from_meters_per_second(
                                            a_y * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                );
                            }
                            Button::ArrowRight => {
                                let dir_angle = rocket.get_direction().as_degrees();
                                rocket.set_direction(measurements::Angle::from_degrees(
                                    dir_angle - (1. / multiplyer as f64),
                                ));
                            }
                            Button::ArrowLeft => {
                                let dir_angle = rocket.get_direction().as_degrees();
                                rocket.set_direction(measurements::Angle::from_degrees(
                                    dir_angle + (1. / multiplyer as f64),
                                ))
                            }
                            Button::Stage => {}
                            Button::Space => {
                                if rocket.is_crashed().is_none() {
                                    rocket.set_flying();
                                }

                                rocket.burn(self.tick_time as f64 / (1_000. * multiplyer as f64));

                                let direction = rocket.get_direction();
                                let a = rocket.get_main_engine_thrust().as_newtons() / mass;

                                let a_x = a * direction.as_radians().cos();
                                let a_y = a * direction.as_radians().sin();
                                let (speed_x, speed_y) = self.objects[focus].get_speed();
                                self.objects[focus].set_speed(
                                    speed_x
                                        + Speed::from_meters_per_second(
                                            a_x * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                    speed_y
                                        - Speed::from_meters_per_second(
                                            a_y * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                );
                            }
                            Button::PanRight => {
                                let direction =
                                    Angle::from_degrees(rocket.get_direction().as_degrees() - 90.);
                                let a = rocket.get_side_engine_thrust().as_newtons() / mass;
                                let a_x = a * direction.as_radians().cos();
                                let a_y = a * direction.as_radians().sin();
                                let (speed_x, speed_y) = self.objects[focus].get_speed();
                                self.objects[focus].set_speed(
                                    speed_x
                                        + Speed::from_meters_per_second(
                                            a_x * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                    speed_y
                                        - Speed::from_meters_per_second(
                                            a_y * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                );
                            }
                            Button::PanLeft => {
                                let direction =
                                    Angle::from_degrees(rocket.get_direction().as_degrees() + 90.);
                                let a = rocket.get_side_engine_thrust().as_newtons() / mass;
                                let a_x = a * direction.as_radians().cos();
                                let a_y = a * direction.as_radians().sin();
                                let (speed_x, speed_y) = self.objects[focus].get_speed();
                                self.objects[focus].set_speed(
                                    speed_x
                                        + Speed::from_meters_per_second(
                                            a_x * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                    speed_y
                                        - Speed::from_meters_per_second(
                                            a_y * self.tick_time as f64
                                                / (1_000. * multiplyer as f64),
                                        ),
                                );
                            }
                        };
                    }
                }
            }
            for p in 0..self.objects.len() {
                if let Some(tracker) = self.objects[p].is_landed() {
                    let object_to_track = self.objects[tracker.object].clone();
                    self.objects[p]
                        .set_pos_tracker(&object_to_track, (tracker.offset.x, tracker.offset.y));
                    let tracker_speed = object_to_track.get_speed();
                    self.objects[p].set_speed(tracker_speed.0, tracker_speed.1);
                }

                if let Some(tracker) = self.objects[p].is_crashed() {
                    let object_to_track = self.objects[tracker.object].clone();
                    self.objects[p]
                        .set_pos_tracker(&object_to_track, (tracker.offset.x, tracker.offset.y));
                    let tracker_speed = object_to_track.get_speed();
                    self.objects[p].set_speed(tracker_speed.0, tracker_speed.1);
                } else {
                    let (fx, fy) = get_net_force(&self.objects, p);
                    if self.objects[p].is_rocket() {
                        self.objects[p].set_force((fx, fy));
                    }
                    let (dx, dy) = self.objects[p].get_new_velocity(
                        (fx, fy),
                        self.tick_time as f64 / (1_000 * multiplyer) as f64,
                    );
                    self.objects[p].set_speed(dx, dy);
                    self.objects[p]
                        .update_pos(self.tick_time as f64 / (1_000. * multiplyer as f64));
                    Object::set_if_crashed(&mut self.objects, p);
                    if let Some(tracker) = self.objects[p].is_landed() {
                        let object_to_track = self.objects[tracker.object].clone();
                        self.objects[p].set_pos_tracker(
                            &object_to_track,
                            (tracker.offset.x, tracker.offset.y),
                        );
                    }
                }
            }
            self.time_ms += self.tick_time / multiplyer;
        }
    }
    pub fn trigger_mouse_up(&mut self, x: i32, y: i32) {
        if self.view == ViewState::Main {
            if let Some(where_mouse_down) = self.where_mouse_down {
                if (x as f64 - where_mouse_down.x.as_millimeters() / self.scale as f64).abs() < 15.
                    && (y as f64 - where_mouse_down.y.as_millimeters() / self.scale as f64).abs()
                        < 15.
                {
                    for index in self.find_object(x, y) {
                        if self.objects[index].is_rocket() {
                            self.focus = Some(index);
                        }
                    }
                }
            };
            self.where_mouse_down = None;
        }
    }
    pub fn is_mouse_down(&mut self) -> bool {
        self.where_mouse_down.is_some()
    }
    pub fn pan(&mut self, x: u32, y: u32) {
        if self.view == ViewState::Main {
            let where_mouse_down = match &self.where_mouse_down {
                Some(it) => it,
                _ => return,
            };
            let x = Length::from_millimeters((x as u64 * self.scale) as f64);
            let y = Length::from_millimeters((y as u64 * self.scale) as f64);
            self.track.offset = Pos {
                x: x - where_mouse_down.x,
                y: y - where_mouse_down.y,
            };
        }
    }
    pub fn trigger_mouse_down(&mut self, menu_visibility: String, x: u32, y: u32) {
        if self.view == ViewState::Main {
            if menu_visibility != "visible" {
                let old_x = (self.track.offset.x.as_millimeters()) as i64;
                let old_y = (self.track.offset.y.as_millimeters()) as i64;
                let tx = x as i64 * self.scale as i64 - old_x;
                let ty = y as i64 * self.scale as i64 - old_y;
                self.where_mouse_down = Some(Pos {
                    x: Length::from_millimeters(tx as f64),
                    y: Length::from_millimeters(ty as f64),
                });
            }
        }
    }
    pub fn should_move(&self) -> bool {
        self.should_move
    }
    fn start(&mut self) {
        if self.view == ViewState::Main {
            self.should_move = true;
        }
    }
    fn stop(&mut self) {
        if self.view == ViewState::Main {
            self.should_move = false;
        }
    }
    pub fn toggle_move(&mut self) {
        if self.should_move() {
            self.stop();
        } else {
            self.start();
        }
    }
    pub fn get_scale_string(&self) -> String {
        if self.scale > 10_000_000 {
            ((self.scale as f64 / 1_000_000.) as u64).to_string() + " km/pixel"
        } else if self.scale > 10_000 {
            ((self.scale as f64 / 1_000.) as u64).to_string() + " m/pixel"
        } else {
            (self.scale as f64).to_string() + " mm/pixel"
        }
    }
    pub fn zoom_in(&mut self, amnt: u32) {
        if self.view == ViewState::Main {
            let _amnt: u64 = amnt.into();
            if self.scale > 4 {
                self.scale = (self.scale as f64 / 1.5) as u64
            }
        }
    }
    pub fn set_focus(&mut self, x: i32, y: i32) {
        if self.view == ViewState::Main {
            for index in self.find_object(x, y) {
                if self.objects[index].is_rocket() {
                    self.focus = Some(index);
                }
            }
        }
    }

    pub fn get_name(&self, id: usize) -> String {
        if self.objects[id].is_crashed().is_some() {
            format!("{} (Crashed)", self.objects[id].get_name())
        } else if self.objects[id].is_landed().is_some() {
            format!("{} (Landed)", self.objects[id].get_name())
        } else {
            self.objects[id].get_name().clone()
        }
    }

    pub fn get_focus_name(&self) -> Option<String> {
        if let Some(focus) = self.focus {
            Some(self.get_name(focus))
        } else {
            None
        }
    }
    pub fn key_up(&mut self, key: String) {
        if let Some(focus) = self.focus {
            if self.objects[focus].is_rocket() {
                match key.as_ref() {
                    "ArrowLeft" => self.buttons_pressed.retain(|b| b != &Button::ArrowLeft),
                    "ArrowRight" => self.buttons_pressed.retain(|b| b != &Button::ArrowRight),
                    " " => self.buttons_pressed.retain(|b| b != &Button::Space),
                    "ArrowUp" => self.buttons_pressed.retain(|b| b != &Button::ArrowUp),
                    "ArrowDown" => self.buttons_pressed.retain(|b| b != &Button::ArrowDown),
                    "<" => self.buttons_pressed.retain(|b| b != &Button::PanLeft),
                    ">" => self.buttons_pressed.retain(|b| b != &Button::PanRight),
                    "t" => self.buttons_pressed.retain(|b| b != &Button::Stage),
                    s => {
                        log!("{}", s);
                    }
                }
            }
        }
    }
    pub fn key_press(&mut self, key: String, cur_time: u64) {
        if self.view == ViewState::Main {
            if key.chars().count() > 0
                && key.chars().nth(0).unwrap() >= '0'
                && key.chars().nth(0).unwrap() <= '9'
            {
                let code = (key.chars().nth(0).unwrap() as u8 - '0' as u8) as u64;
                self.tick_time = code * 100 / 6;
            }
            match key.as_ref() {
                "p" => {
                    self.toggle_move();
                }
                "s" => {
                    self.save_data(cur_time);
                }
                "o" => {
                    self.set_load(cur_time);
                }
                "?" => {
                    self.set_view(ViewState::Help);
                }
                "PageUp" => {
                    self.speed_up(true);
                }
                "PageDown" => {
                    self.speed_up(false);
                }
                "+" => {
                    self.zoom_in(10000);
                }
                "-" => {
                    self.zoom_out(10000);
                }
                s => {
                    log!("{}", s);
                }
            }
            if let Some(focus) = self.focus {
                if self.objects[focus].is_rocket() {
                    if self.objects[focus].is_flying() {
                        match key.as_ref() {
                            "ArrowLeft" => {
                                if !self.buttons_pressed.contains(&Button::ArrowLeft) {
                                    self.buttons_pressed.push(Button::ArrowLeft);
                                }
                            }
                            "ArrowRight" => {
                                if !self.buttons_pressed.contains(&Button::ArrowRight) {
                                    self.buttons_pressed.push(Button::ArrowRight);
                                }
                            }
                            " " => {
                                if !self.buttons_pressed.contains(&Button::Space) {
                                    self.buttons_pressed.push(Button::Space);
                                }
                                //Fire SuperDracos
                            }
                            "ArrowUp" => {
                                //Fire Dracos Forward
                                if !self.buttons_pressed.contains(&Button::ArrowUp) {
                                    self.buttons_pressed.push(Button::ArrowUp);
                                }
                            }
                            "ArrowDown" => {
                                if !self.buttons_pressed.contains(&Button::ArrowDown) {
                                    self.buttons_pressed.push(Button::ArrowDown);
                                }
                            }
                            "<" => {
                                if !self.buttons_pressed.contains(&Button::PanLeft) {
                                    self.buttons_pressed.push(Button::PanLeft);
                                }
                            }
                            ">" => {
                                if !self.buttons_pressed.contains(&Button::PanRight) {
                                    self.buttons_pressed.push(Button::PanRight);
                                }
                            }
                            "t" => {
                                let new_object = self.objects[focus].stage();
                                let is_staged = if let Some(new_object) = new_object {
                                    self.objects.push(new_object);
                                    true
                                } else {
                                    false
                                };
                                if is_staged {
                                    if !self.buttons_pressed.contains(&Button::ArrowUp) {
                                        self.buttons_pressed.push(Button::ArrowUp);
                                    }
                                }
                            }
                            s => {
                                log!("{}", s);
                            }
                        }
                    } else if self.objects[focus].is_landed().is_some() {
                        // Once you land, you can only go up
                        match key.as_ref() {
                            " " => {
                                if !self.buttons_pressed.contains(&Button::Space) {
                                    self.buttons_pressed.push(Button::Space);
                                }
                                //Fire SuperDracos
                            }
                            "ArrowUp" => {
                                //Fire Dracos Forward
                                if !self.buttons_pressed.contains(&Button::ArrowUp) {
                                    self.buttons_pressed.push(Button::ArrowUp);
                                }
                            }

                            s => {
                                log!("{}", s);
                            }
                        }
                    }
                }
            }
        } else if key == "Escape" {
            self.set_view(ViewState::Main);
        }
    }

    fn set_view(&mut self, state: ViewState) {
        match state {
            ViewState::Main => {
                self.view = ViewState::Main;
            }
            ViewState::Load { .. } => {
                if self.view == ViewState::Main {
                    if self.should_move {
                        self.toggle_move();
                    }
                    self.view = state;
                }
            }
            ViewState::Help => {
                if self.view == ViewState::Main {
                    if self.should_move {
                        self.toggle_move();
                    }
                    self.view = ViewState::Help;
                }
            }
        }
    }
    pub fn get_view(&self) -> String {
        match self.view {
            ViewState::Help => "Help",
            ViewState::Load { .. } => "Load",
            ViewState::Main => "Main",
        }
        .to_string()
    }
    pub fn save_data(&self, cur_time: u64) {
        if self.view == ViewState::Main {
            let window = web_sys::window().unwrap();
            let storage = window.session_storage().unwrap();
            if let Some(storage) = storage {
                storage
                    .set_item(
                        &cur_time.to_string(),
                        &serde_json::to_string(&self).unwrap(),
                    )
                    .unwrap();
            } else {
                unreachable!();
            }
        }
    }
    pub fn set_load(&mut self, date_time: u64) {
        self.set_view(ViewState::Load { date_time });
    }
    pub fn clear_saved_data(&mut self) {
        let window = web_sys::window().unwrap();
        let storage = window.session_storage().unwrap();
        if let Some(storage) = storage {
            storage.clear().unwrap();
        }
    }
    pub fn load_game(&mut self, id: String) {
        if let ViewState::Load { .. } = self.view {
            let window = web_sys::window().unwrap();
            let storage = window.session_storage().unwrap();
            if let Some(storage) = storage {
                let id_u64: u64 = id.parse().unwrap();
                let mut what_to_delete = vec![];
                for i in 0..storage.length().unwrap() {
                    let k = storage
                        .key(i)
                        .expect(&format!("Can't get storage key with id {} part 1", i))
                        .expect(&format!("Can't get storage key with id {} part 2", i));
                    let k_u64: u64 = k.parse().expect(&format!("Can't parse item {}", k));
                    if k_u64 > id_u64 {
                        what_to_delete.push(k);
                    }
                }
                for k in what_to_delete {
                    storage.delete(&k).unwrap();
                }
                let v = storage.get(&id.to_string()).unwrap().unwrap();

                *self = serde_json::from_str(&v).unwrap();
                self.stop();
                self.buttons_pressed = vec![];
            } else {
                unreachable!();
            }
        } else {
            unreachable!();
        }
    }
    pub fn get_load_data(&self) -> String {
        if let ViewState::Load { .. } = self.view {
            let window = web_sys::window().unwrap();
            let storage = window.session_storage().unwrap();
            if let Some(storage) = storage {
                let mut data: Vec<(String, String)> = (0..storage.length().unwrap())
                    .map(|i| {
                        let key = storage.key(i).unwrap().unwrap();
                        let v = storage
                            .get(&storage.key(i).unwrap().unwrap())
                            .unwrap()
                            .unwrap();
                        let deserialized: Universe = serde_json::from_str(&v).unwrap();
                        (key.to_string(), deserialized.time_ms.to_string())
                    })
                    .collect();
                data.sort_by(|a, b| {
                    let a_1: u64 = a.1.parse().unwrap();
                    let b_1: u64 = b.1.parse().unwrap();
                    b_1.cmp(&a_1)
                });
                json!({ "data": data }).to_string()
            } else {
                unreachable!();
            }
        } else {
            String::new()
        }
    }
    pub fn set_help(&mut self) {
        self.set_view(ViewState::Help);
    }
    pub fn set_main(&mut self) {
        self.set_view(ViewState::Main);
    }
    pub fn get_fuel_mass(&self) -> String {
        if let Some(focus) = self.focus {
            if let Some(rocket) = self.objects[focus].get_rocket() {
                format!("{:.03} kg", rocket.get_fuel_mass().as_kilograms())
            } else {
                "".into()
            }
        } else {
            "".into()
        }
    }
    pub fn get_speed_of_movement(&self) -> String {
        if let Some(velocity_to) = self.velocity_to {
            if let Some(focus) = self.focus {
                let rocket_speed = &self.objects[focus].get_speed();
                let obj = &self.objects[velocity_to];

                let (obj_x, obj_y) = obj.get_speed();
                let diff_x = rocket_speed.0 - obj_x;
                let diff_y = rocket_speed.1 - obj_y;
                let relative_speed = (diff_x.as_meters_per_second()
                    * diff_x.as_meters_per_second()
                    + diff_y.as_meters_per_second() * diff_y.as_meters_per_second())
                .sqrt();
                if relative_speed as u64 / 1000 == 0 {
                    return format!("{:.3} m/s relative to {}\n", relative_speed, obj.get_name());
                } else {
                    return format!(
                        "{:.3} km/s relative to {}\n",
                        relative_speed / 1000.,
                        obj.get_name()
                    );
                }
            };
        };
        "".into()
    }
    pub fn zoom_out(&mut self, _amnt: u32) {
        if self.view == ViewState::Main {
            self.scale = (self.scale as f64 * 1.5) as u64;
        }
    }
    pub fn get_date(&self) -> u64 {
        self.time_ms
    }
    pub fn get_tick_time(&self) -> String {
        let ms = self.tick_time * 60 % 1000;
        let s = (self.tick_time * 60 / 1000) % 60;
        let m = (self.tick_time * 60 / (1000 * 60)) % 60;
        let h = (self.tick_time * 60 / (1000 * 60 * 60)) % 24;
        let d = self.tick_time * 60 / (1000 * 60 * 60 * 24);
        format!("{}d {}h {}m {}s {}ms per s", d, h, m, s, ms)
    }
    pub fn speed_up(&mut self, up :bool) {
        if self.view == ViewState::Main {
            if up {
                self.tick_time += 960;
            } else { 
                self.tick_time -= 960;
            } 
        }
    }

    pub fn slow_down(&mut self) {
        if self.view == ViewState::Main {
            if self.tick_time >= 1_000 * 60 {
                self.tick_time -= 1_000 * 60;
            }
        }
    }
    pub fn get_images(&self) -> JsValue {
        let v: Vec<Vec<ImageData>> = self
            .objects
            .iter()
            .enumerate()
            .map(|(i, obj)| {
                let mut img = obj.get_image(self.scale as f64);
                let is_focused = if let Some(focus) = self.focus {
                    if focus == i {
                        true
                    } else {
                        false
                    }
                } else {
                    false
                };
                for obj in &mut img {
                    obj.is_focused = is_focused;
                }
                img
            })
            .collect();
        let sv = StringVec(v);
        JsValue::from_str(&serde_json::to_string(&sv).unwrap())
    }
    pub fn get_distance(&self) -> Option<String> {
        if let Some(focus) = self.focus {
            if let Some(velocity_to) = self.velocity_to {
                let x_diff = (self.objects[focus].get_position().x
                    - self.objects[velocity_to].get_position().x)
                    .as_meters();
                let y_diff = (self.objects[focus].get_position().y
                    - self.objects[velocity_to].get_position().y)
                    .as_meters();
                let dist = (x_diff * x_diff + y_diff * y_diff).sqrt();
                let dist_with_radius =
                    dist - self.objects[velocity_to].get_radius().unwrap().as_meters();
                Some(if dist_with_radius / AU > 1. {
                    format!("{:.3} AU", dist_with_radius / AU)
                } else if dist_with_radius / 1000. > 10. {
                    format!("{:.3} km", dist_with_radius / 1000.)
                } else if dist_with_radius > 10. {
                    format!("{:.3} m", dist_with_radius)
                } else {
                    format!("{:.3} cm", dist_with_radius / 100.)
                })
            } else {
                None
            }
        } else {
            None
        }
    }
    pub fn get_direction_pointer(&self) -> Option<String> {
        if let Some(focus) = self.focus {
            if let Some(velocity_to) = self.velocity_to {
                let x_orig = self.get_object_position(focus)?.x;
                let y_orig = self.get_object_position(focus)?.y;
                let v = self.objects[focus].get_velocity();
                let new_x: f64 = x_orig as f64
                    + (v.speed_x - self.objects[velocity_to].get_velocity().speed_x)
                        .as_meters_per_second();
                let new_y: f64 = y_orig as f64
                    + (v.speed_y - self.objects[velocity_to].get_velocity().speed_y)
                        .as_meters_per_second();
                Some(
                    serde_json::to_string(&(
                        x_orig as i32,
                        y_orig as i32,
                        new_x as i32,
                        new_y as i32,
                    ))
                    .unwrap(),
                )
            } else {
                None
            }
        } else {
            None
        }
    }
    pub fn get_acceleration_pointer(&self) -> Option<String> {
        if let Some(focus) = self.focus {
            if let Some((a_x, a_y)) = self.objects[focus].get_force() {
                let x_orig = self.get_object_position(focus)?.x;
                let y_orig = self.get_object_position(focus)?.y;

                let new_x: f64 =
                    x_orig as f64 + (a_x / self.objects[focus].get_mass().as_kilograms() * 10.);
                let new_y: f64 =
                    y_orig as f64 + (a_y / self.objects[focus].get_mass().as_kilograms() * 10.);

                Some(
                    serde_json::to_string(&(
                        x_orig as i32,
                        y_orig as i32,
                        new_x as i32,
                        new_y as i32,
                    ))
                    .unwrap(),
                )
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct ImageData {
    t: u8,
    radius: u64,
    size: u16,
    color: String,
    points: Vec<i32>,
    direction_dist: f64,
    direction_x: f64,
    direction_y: f64,
    show_guide: bool,
    is_focused: bool,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct StringVec(Vec<Vec<ImageData>>);

fn distance(x: f64, y: f64) -> f64 {
    ((x * x) + (y * y)).sqrt()
}

pub(crate) fn get_net_force(planets: &[Object], index: usize) -> (f64, f64) {
    let c = &planets;
    let object = &c[index];
    let mut total_fx = 0.;
    let mut total_fy = 0.;
    for (j, other) in planets.iter().enumerate() {
        if index != j {
            let (fx, fy) = get_gravitational_force_in_newtons(
                object.get_mass(),
                other.get_mass(),
                object.get_position(),
                other.get_position(),
            );
            total_fx += fx;
            total_fy += fy;
        }
    }
    (total_fx, total_fy)
}

pub(crate) fn get_gravitational_force_in_newtons(
    self_mass: Mass,
    other_mass: Mass,
    self_pos: Pos,
    other_pos: Pos,
) -> (f64, f64) {
    let (sx, sy) = (self_pos.x, self_pos.y);
    let (ox, oy) = (other_pos.x, other_pos.y);
    let dx = (ox - sx).as_meters();
    let dy = (oy - sy).as_meters();

    let d = (dx * dx + dy * dy).sqrt();
    if d != 0. {
        let f = G * self_mass.as_kilograms() * other_mass.as_kilograms() / (d * d);
        let theta = dy.atan2(dx);
        let fx = (theta).cos() * f;
        let fy = (theta).sin() * f;
        (fx, fy)
    } else {
        (0., 0.)
    }
}

mod test {
    #[test]
    fn test_acceleration() {
        use crate::CrashState;
        use crate::Object;
        use crate::ObjectType;
        use crate::Pos;
        use crate::Rocket;
        use crate::Universe;
        use crate::Velocity;
        use measurements::Angle;
        use measurements::Force;
        use measurements::Length;
        use measurements::Mass;
        use measurements::Speed;

        let mut u = Universe::init(10, 1000000000000, 100, 100, 1000);
        u.objects = vec![Object::new(
            String::from("test rocket"),
            ObjectType::Rocket(Rocket::new(
                Angle::from_degrees(0.),
                Force::from_newtons(10.),
                Force::from_newtons(0.),
                Mass::from_kilograms(1.),
                CrashState::Flying,
                100,
                vec![],
                vec![],
            )),
            Pos {
                x: Length::from_kilometers(0.),
                y: Length::from_kilometers(0.),
            },
            Mass::from_kilograms(0.),
            Velocity {
                speed_x: Speed::from_meters_per_second(0.),
                speed_y: Speed::from_meters_per_second(0.),
            },
        )];
        u.focus = Some(0);
        u.key_press("p".to_string(), 0);
        for _ in 0..10 {
            u.key_press(" ".to_string(), 0);
            u.tick();
        }
        panic!("{:?}", u.objects[0].position);
    }
}
