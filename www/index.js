/* global BigInt */
import { Universe } from "rbit"

let dpi = window.devicePixelRatio;
function fix_dpi() {
    //create a style object that returns width and height
    let style = {
        height() {
            return getComputedStyle(canvas).getPropertyValue('height').slice(0, -2);
        },
        width() {
            return getComputedStyle(canvas).getPropertyValue('width').slice(0, -2);
        }
    }
    //set the correct attributes for a crystal clear image!
    canvas.setAttribute('width', style.width() * dpi);
    canvas.setAttribute('height', style.height() * dpi);
}

const loadButton = (id) => {
    universe.load_game(id);
}
const canvas = document.getElementById("main_canvas");
const ctx = canvas.getContext('2d');
const universe = Universe.init(BigInt(10), BigInt("1000"), canvas.height, canvas.width, BigInt(60 * 60));
universe.clear_saved_data();
let oldDataString = [];
fix_dpi();

const whenFinishedLoadingAssets = function () {
    universe.set_help();
    const renderLoop = () => {
        ctx.lineWidth = 1;

        universe.set_canvas_size(canvas.width, canvas.height);

        const v = universe.get_view();
        switch (v) {
            case "Main": {
                canvas.focus(); document.getElementById("helpPage").style.visibility = 'hidden';
                document.getElementById("loadPage").style.visibility = 'hidden';
                break;
            }
            case "Help": {
                document.getElementById("helpPage").style.visibility = 'visible';
                break;
            }
            case "Load": {
                universe.get_load_data();
                const dataString = universe.get_load_data(BigInt(new Date()) / BigInt(1000));
                if (dataString != oldDataString) {
                    oldDataString = dataString;
                    const loadData = JSON.parse(dataString);
                    let innerTable = document.createElement("table");
                    let ids = [];
                    const headerRow = document.createElement("tr");
                    const headerCol1 = document.createElement("td");
                    headerCol1.innerText = "Real time of save";
                    const headerCol2 = document.createElement("td");
                    headerCol2.innerText = "In-game time of save";
                    const headerCol3 = document.createElement("td");
                    headerCol3.innerText = "";
                    headerRow.appendChild(headerCol1);
                    headerRow.appendChild(headerCol2);
                    headerRow.appendChild(headerCol3);
                    innerTable.appendChild(headerRow);
                    loadData.data.forEach((d, i) => {
                        const innerRow = document.createElement("tr");
                        const innerCol1 = document.createElement("td");
                        innerCol1.innerText = new Date(d[0] * 1000).toLocaleString();
                        const innerCol2 = document.createElement("td");
                        innerCol2.innerText = new Date(d[1] * 1000).toLocaleString();

                        ids.push(`loadButton-${d[0]}-${d[1]}`);
                        const innerCol3 = document.createElement("td");
                        if (i == loadData.data.length - 1) {
                            innerCol1.setAttribute("class", "last-row");
                            innerCol2.setAttribute("class", "last-row");
                            innerCol3.setAttribute("class", "last-row");
                        }
                        const button = document.createElement("button");
                        button.setAttribute("class", "button");
                        button.setAttribute("style", "color:bisque");
                        button.setAttribute("id", `loadButton-${d[0]}`);
                        button.innerText = "load";
                        button.addEventListener('click', function () {
                            loadButton(d[0]);
                        }, false);
                        innerCol3.appendChild(button);
                        innerRow.appendChild(innerCol1);
                        innerRow.appendChild(innerCol2);
                        innerRow.appendChild(innerCol3);
                        innerTable.appendChild(innerRow);

                    });

                    removeAllChildNodes(document.getElementById("savedGames"));
                    document.getElementById("savedGames").appendChild(innerTable);



                }
                document.getElementById("loadPage").style.visibility = 'visible';
                break;
            }

        }



        const date = new Date(Number(universe.get_date()));
        ctx.fillStyle = "white";
        ctx.font = "15px Arial";
        document.getElementById("timeSpot").innerHTML = date.toLocaleTimeString("en-us", { year: "numeric", month: "short", day: "numeric" }) + " <br> " + universe.get_tick_time();
        document.getElementById("scaleString").innerHTML = universe.get_scale_string();
        document.getElementById("fuelAmount").innerText = universe.get_fuel_mass();

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        const gi = JSON.parse(universe.get_images())

        gi.forEach((img, i) => {
            img.forEach((img, j) => {
                const pos = universe.get_object_position(i);
                if (pos != undefined) {
                    if (img.t == 0) {
                        ctx.beginPath()

                        ctx.arc(pos.x, pos.y, img.radius, 0, 2 * Math.PI);
                        ctx.fillStyle = img.color;
                        ctx.fill();
                    } else if (img.t == 1) {
                        const x = pos.x
                        const y = pos.y
                        const points = img.points;
                        ctx.beginPath();
                        ctx.moveTo(points[0] + x, points[1] + y);
                        points.splice(0, 2);
                        ctx.strokeStyle = "lightgrey";
                        for (let j = 0; j < points.length; j += 2) {
                            ctx.lineTo(points[j] + x, points[j + 1] + y);
                        }
                        ctx.closePath();
                        ctx.fillStyle = "white";
                        ctx.fill();
                        if (img.show_guide) {
                            ctx.beginPath();
                            if (img.is_focused) {
                                ctx.strokeStyle = '#00AAAA';
                            } else {
                                ctx.strokeStyle = '#00FF00';
                            }
                            ctx.arc(x, y, img.direction_dist, 0, 2 * Math.PI);
                            ctx.stroke();
                            ctx.beginPath();
                            ctx.arc(img.direction_x + x, img.direction_y + y, 2, 0, 2 * Math.PI);
                            ctx.fill();
                        }
                    }
                }
            })
        });
        const focusName = universe.get_focus_name();

        if (focusName !== undefined) {
            document.getElementById("focusName").innerHTML = focusName;
            document.getElementById("speedOfMovement").innerHTML = universe.get_speed_of_movement();
        }
        const accelerationPointer = universe.get_acceleration_pointer();
        if (accelerationPointer !== undefined) {
            console.log(accelerationPointer)
            const p = JSON.parse(accelerationPointer);
            ctx.strokeStyle = '#FF0000';
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.moveTo(p[0], p[1]);
            ctx.lineTo(p[2], p[3]);
            ctx.closePath();
            ctx.stroke();
        }
        const directionPointer = universe.get_direction_pointer();

        if (directionPointer !== undefined) {
            const p = JSON.parse(directionPointer);
            ctx.strokeStyle = '#00FFFF';
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.moveTo(p[0], p[1]);
            ctx.lineTo(p[2], p[3]);
            ctx.closePath();
            ctx.stroke();
            document.getElementById("distance").innerHTML = universe.get_distance();
        }
        if (universe.should_move()) {
            document.getElementById("stop").style.display = "inherit";
            document.getElementById("move").style.display = "none";
            universe.tick();
        } else {
            document.getElementById("stop").style.display = "none";
            document.getElementById("move").style.display = "inherit";

        }

        requestAnimationFrame(renderLoop);
    }
    requestAnimationFrame(renderLoop);
}



whenFinishedLoadingAssets();


const toggleMove = () => {
    universe.toggle_move();
}
const zoomIn = () => {
    universe.zoom_in(10000);
}
const zoomOut = () => {
    universe.zoom_out(10000);
}

const speedUp = () => {
    universe.speed_up(true);
}
const slowDown = () => {
    universe.speed_up(false);
}

const zoomInButton = () => {
    universe.zoom_in(100);
}
const zoomOutButton = () => {
    universe.zoom_out(100);
}

const center_at = (a) => {
    universe.center_at(a);
}


const v_to = (a) => {
    universe.v_to(a);
}

const help = () => {
    universe.set_help();
}

const closeHelpPage = () => {
    universe.set_main();
}

const closeLoadPage = () => {
    universe.set_main();
}

const runSave = () => {
    universe.save_data(BigInt(new Date()) / BigInt(1000));
}
const runClear = () => {
    universe.clear_saved_data();
}
const setLoad = () => {
    universe.set_load(BigInt(new Date()) / BigInt(1000));
}



document.getElementById("stopMove").onclick = toggleMove;
document.getElementById("zoomIn").onclick = zoomInButton;
document.getElementById("zoomOut").onclick = zoomOutButton;
document.getElementById("speedUp").onclick = speedUp;
document.getElementById("slowDown").onclick = slowDown;
document.getElementById("help").onclick = help;
document.getElementById("saveButton").onclick = runSave;
document.getElementById("closeLoadButton").onclick = closeLoadPage;
document.getElementById("openButton").onclick = setLoad;
document.getElementById("clearSavedGames").onclick = runClear;
document.getElementById("closeHelpPage").onclick = closeHelpPage;

var i = document.getElementById("menu").style;

canvas.addEventListener('mousedown', function (e) {
    if (e.buttons === 1) {
        const mousePos = getMousePos(canvas, e);
        universe.trigger_mouse_down(i.visibility, mousePos.x, mousePos.y)
    }
})
canvas.addEventListener('mouseup', function (e) {
    const mousePos = getMousePos(canvas, e);
    universe.trigger_mouse_up(mousePos.x, mousePos.y)
})
canvas.addEventListener('wheel', function (event) {
    if (event.deltaY < 0) {
        zoomIn()
    } else {
        zoomOut()
    }
    return false;
}, false);
document.addEventListener('keydown', function (evt) {
    console.log("down", evt);
    universe.key_press(evt.key, BigInt(new Date()) / BigInt(1000))
}, false);
document.addEventListener('keyup', function (evt) {
    console.log("up", evt);
    universe.key_up(evt.key)
}, false);
canvas.addEventListener('mousemove', function (evt) {
    const mousePos = getMousePos(canvas, evt);
    const foundObject = universe.find_object(mousePos.x, mousePos.y);
    if (foundObject.length != 0) {
        let innerHTML = "";
        for (let i = 0; i < foundObject.length; i++) {
            innerHTML += `${universe.get_name(foundObject[i])}<br/>`;
        }
        document.getElementById("objectsName").innerHTML = innerHTML;
        document.body.style.cursor = 'crosshair';
    } else {
        document.getElementById("objectsName").innerHTML = "";
        document.body.style.cursor = 'default';
    }
    if (universe.is_mouse_down()) {
        document.body.style.cursor = 'default';
        universe.pan(mousePos.x, mousePos.y)
    }
}, false);
//https://stackoverflow.com/questions/17130395/real-mouse-position-in-canvas
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: (evt.clientX - rect.left) / (rect.right - rect.left) * canvas.width,
        y: (evt.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height
    };
}

//https://stackoverflow.com/questions/4909167/how-to-add-a-custom-right-click-menu-to-a-webpage
canvas.addEventListener('contextmenu', function (e) {

    menu(e);
    e.preventDefault();
}, false);


document.addEventListener('click', function (e) {
    i.opacity = "0";
    setTimeout(function () {
        i.visibility = "hidden";
    }, 501);
}, false);

function menu(evt) {
    var i = document.getElementById("menu").style;
    const pos = getMousePos(canvas, evt);
    const x = pos.x;
    const y = pos.y;
    const contextMenuText = universe.context_menu_to_show(x, y);
    if (contextMenuText !== undefined) {
        const s = contextMenuText.split(";")
        let t_arr = [];
        const p = s.map((s) => {
            if (s != "") {
                const t = s.split("|");
                t_arr.push(t[1]);
                return `<a href="#" id='link_${t_arr.length - 1}' ">${t[0]}</a>`
            }
        }).join("");
        document.getElementById("menu").innerHTML = p;
        for (let i = 0; i < t_arr.length; i++) {
            let link = document.getElementById(`link_${i}`);
            link.addEventListener('click', () => { eval(t_arr[i]); });
        }
        i.top = evt.y + "px";
        i.left = evt.x + "px";
        i.visibility = "visible";
        i.opacity = "1";
    } else {
        var i = document.getElementById("menu").style;
        i.opacity = "0";
        setTimeout(function () {
            i.visibility = "hidden";
        }, 501);
    }
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}
