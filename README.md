rbit
-----
rbit is an orbital simulator written in rust.

## Online version

You can play the online version at [https://https://rbit.netlify.app/](https://rbit.netlify.app/).

## Instructions
Go to the [game](https://https://rbit.netlify.app/) and click on the question mark.
## Building

You need to install npm and rustup before running the following:
```
rustup toolchain install stable
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh 
wasm-pack build
cd www
npm install
npm run build ./node_modules/.bin/webpack
```

## License

MIT or APACHE-2.0